package codeminders.linecounting.linesprecessing;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Test;

public final class NoCommentBlockLinesTest {

  @Test
  public void list() {
    // given
    final List<String> lines = Arrays.asList(
        "",
        "// /* A line comment",
        "/*",
        "A comment",
        "*/",
        "/* Here",
        "This is a test",
        "*/",
        "public class Hello {",
        "  int i =0; /*",
        "  Start after code",
        "  */",
        "  // Say hello",
        "  System.out.println(\"Hello\");",
        "}"
    );

    final String expected =
        ":// /* A line comment"
            + "::::::::public class Hello {:"
            + "int i =0;::::"
            + "  // Say hello:  System.out.println(\"Hello\");:}";

    // when
    final String actual = new NoCommentBlockLines(lines.stream())
        .collect(Collectors.joining(":"));

    // then
    assertEquals(expected, actual);
  }
}
