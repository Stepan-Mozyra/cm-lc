package codeminders.linecounting.linesprecessing;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Test;

public class PreparedLinesTest {

  @Test
  public void prepare() {
    // given
    final List<String> lines = Arrays.asList(
        "",
        "// /* A line comment",
        "/*",
        "A comment",
        "*/",
        "public class Hello {",
        "  // Say \\\"hello",
        "  System./*wait*/out.println(\"Hello\");",
        "}"
    );

    final String expected =
        ":// /* A line comment:/*:A comment:*/"
            + ":public class Hello {:  // Say hello:  System.out.println();:}";

    // when
    final String actual = new PreparedLines(lines.stream())
        .collect(Collectors.joining(":"));

    // then
    assertEquals(expected, actual);
  }
}
