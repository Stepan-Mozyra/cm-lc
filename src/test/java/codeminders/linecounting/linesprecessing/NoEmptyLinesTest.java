package codeminders.linecounting.linesprecessing;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Test;

public final class NoEmptyLinesTest {

  @Test
  public void lines() {
    // given
    final List<String> lines = Arrays.asList(
        "",
        "// /* A line comment",
        "  ",
        "\n",
        "public class Hello {",
        "  // Say hello",
        "  System.out.println(\"Hello\");",
        "}",
        " "
    );

    final String expected =
        "// /* A line comment:public class Hello {:// Say hello:System.out.println(\"Hello\");:}";

    // when
    final String actual = new NoEmptyLines(lines.stream())
        .collect(Collectors.joining(":"));

    // then
    assertEquals(expected, actual);
  }
}
