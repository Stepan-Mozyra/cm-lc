package codeminders.linecounting.linesprecessing;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class NoEscapingQuoteLineTest {

  @Test
  public void testToString() {
    // given
    final String given = "final String given = \"abc \\\"def\\\" ghi\"";

    final String expected = "final String given = \"abc def ghi\"";

    // when
    final String actual = new NoEscapingQuoteLine(given).toString();

    // then
    assertEquals(expected, actual);
  }
}
