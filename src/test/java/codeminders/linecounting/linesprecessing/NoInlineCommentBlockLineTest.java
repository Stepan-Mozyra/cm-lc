package codeminders.linecounting.linesprecessing;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class NoInlineCommentBlockLineTest {

  @Test
  public void testToString() {
    // given
    final String given = "System./*wait*/out./*for*/println/*it*/(\"Hello/*\");";

    final String expected = "System.out.println(\"Hello/*\");";

    // when
    final String actual = new NoInlineCommentBlockLine(given).toString();

    // then
    assertEquals(expected, actual);
  }
}
