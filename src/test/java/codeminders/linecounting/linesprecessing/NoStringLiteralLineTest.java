package codeminders.linecounting.linesprecessing;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class NoStringLiteralLineTest {

  @Test
  public void testToString() {
    // given
    final String given = "System./*wait*/out./*for*/println/*it*/(\"Hello/*\");";

    final String expected = "System./*wait*/out./*for*/println/*it*/();";

    // when
    final String actual = new NoStringLiteralLine(given).toString();

    // then
    assertEquals(expected, actual);
  }
}
