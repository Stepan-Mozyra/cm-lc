package codeminders.linecounting.linesprecessing;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Test;

public final class NoCommentLinesTest {

  @Test
  public void lines() {
    // given
    final List<String> lines = Arrays.asList(
        "",
        "// /* A line comment",
        "/*",
        "A comment",
        "*/",
        "public class Hello {",
        "  // Say hello",
        "  System.out.println(\"Hello\");",
        "}"
    );

    final String expected =
        ":/*:A comment:*/:public class Hello {:  System.out.println(\"Hello\");:}";

    // when
    final String actual = new NoCommentLines(lines.stream())
        .collect(Collectors.joining(":"));

    // then
    assertEquals(expected, actual);
  }
}
