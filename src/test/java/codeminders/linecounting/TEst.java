package codeminders.linecounting;

import codeminders.linecounting.linesprecessing.NoEmptyLines;
import codeminders.linecounting.linesprecessing.PreparedLines;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import org.junit.Test;

public final class TEst {

  @Test
  public void test() throws IOException {
    Files.readAllLines(
        Paths.get(
            "/Users/stepan/Projects/codeminders/linecounting/src/main/java/codeminders/linecounting/Help.java"),
        StandardCharsets.UTF_8
    )
        .stream()
        .map(s -> s.replaceAll("\\\\\"", ""))
        .map(s -> s.replaceAll("\\\".*?\\\"", ""))
        .map(s -> s.replaceAll("/\\*.*?\\*/", ""))
        .map(s -> s.trim())
        .filter(s -> !s.isEmpty())
        .forEach(s -> System.out.println(s));
  }

  @Test
  public void test2() throws IOException {
    new NoEmptyLines(
        new PreparedLines(
            Files.readAllLines(
                Paths.get(
                    "/Users/stepan/Projects/codeminders/linecounting/src/main/java/codeminders/linecounting/Help.java"
                ),
                StandardCharsets.UTF_8
            )
                .stream()
        )
    )
        .forEach(s -> System.out.println(s));
  }

  // line comment /* some test
  // line comment */ some test
  @Test
  public void test3() {
    final String s = "abc/*def";
    System.out.println(
        s.substring(0, s.indexOf("/*"))
    );
    System.out.println(
        s.substring(s.indexOf("/*"))
    );
  }

}
