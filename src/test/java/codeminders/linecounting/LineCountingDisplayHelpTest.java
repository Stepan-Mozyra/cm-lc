package codeminders.linecounting;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import org.junit.Test;

public final class LineCountingDisplayHelpTest {

  @Test
  public void displayHelpWithEmptyArgs() {
    // given
    final Displayable helpMock = mock(Displayable.class);
    final Arguments arguments = mock(Arguments.class);
    when(arguments.noArgProvided()).thenReturn(true);

    final LineCounting lineCounting = new LineCounting(
        arguments,
        mock(Displayable.class),
        helpMock
    );

    // when
    lineCounting.display(System.out);

    // then
    verify(helpMock, only()).display(System.out);
  }

  @Test
  public void displayHelpWithArgument() {
    // given
    final Displayable helpMock = mock(Displayable.class);
    final Arguments lineCounterMock = mock(Arguments.class);
    when(lineCounterMock.noArgProvided()).thenReturn(false);
    when(lineCounterMock.helpRequired()).thenReturn(true);

    final LineCounting lineCounting = new LineCounting(
        lineCounterMock,
        mock(Displayable.class),
        helpMock
    );

    // when
    lineCounting.display(System.out);

    // then
    verify(helpMock, only()).display(System.out);
  }

  @Test
  public void doNotDisplayHelp() {
    // given
    final Displayable helpMock = mock(Displayable.class);
    final Arguments lineCounterMock = mock(Arguments.class);
    when(lineCounterMock.noArgProvided()).thenReturn(false);
    when(lineCounterMock.helpRequired()).thenReturn(false);

    final LineCounting lineCounting = new LineCounting(
        lineCounterMock,
        mock(Displayable.class),
        helpMock
    );

    // when
    lineCounting.display(System.out);

    // then
    verifyNoInteractions(helpMock);
  }
}
