package codeminders.linecounting;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;

public final class StringLineCounterTest {

  @Test
  public void count() {
    // given
    final List<String> given = Arrays.asList(
        "/*****",
        " * This is a test program with 5 lines of code",
        " *  \\/* no nesting allowed!",
        " //*****//***/// Slightly pathological comment ending...",
        "",
        "public class Hello {",
        "    public static final void main(String [] args) { // gotta love Java",
        "        // Say hello",
        "      System./*wait*/out./*for*/println/*it*/(\"Hello/*\");",
        "    }",
        "",
        "}"
    );

    // when
    final LineCount count = new StringLineCounter(given).count();

    // then
    assertEquals(5, count.number());
  }
}
