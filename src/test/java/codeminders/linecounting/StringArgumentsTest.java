package codeminders.linecounting;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public final class StringArgumentsTest {

  @Test
  public void noArgProvided() {
    // given
    final StringArguments arguments = new StringArguments(new String[]{});

    // when
    final boolean noArgProvided = arguments.noArgProvided();

    // then
    assertTrue(noArgProvided);
  }

  @Test
  public void noArgProvidedFilteredEmpty() {
    // given
    final StringArguments arguments = new StringArguments(new String[]{" "});

    // when
    final boolean noArgProvided = arguments.noArgProvided();

    // then
    assertTrue(noArgProvided);
  }

  @Test
  public void helpRequiredOnly() {
    // given
    final StringArguments arguments = new StringArguments(new String[]{"--help"});

    // when
    final boolean helpRequired = arguments.helpRequired();

    // then
    assertTrue(helpRequired);
  }

  @Test
  public void helpRequired() {
    // given
    final StringArguments arguments = new StringArguments(new String[]{"--help", "/"});

    // when
    final boolean helpRequired = arguments.helpRequired();

    // then
    assertTrue(helpRequired);
  }
}
