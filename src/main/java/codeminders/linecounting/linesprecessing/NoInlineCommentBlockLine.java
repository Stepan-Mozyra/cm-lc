package codeminders.linecounting.linesprecessing;

public final class NoInlineCommentBlockLine {

  private final String line;

  public NoInlineCommentBlockLine(final String line) {
    this.line = line;
  }

  @Override
  public String toString() {
    return
        line.replaceAll("/\\*.*?\\*/", "");
  }
}
