package codeminders.linecounting.linesprecessing;

import codeminders.linecounting.decorators.StringStreamDecorator;
import java.util.stream.Stream;

public final class NoCommentLines extends StringStreamDecorator {

  private final static String LINE_COMMENT = "//";

  public NoCommentLines(final Stream<String> stream) {
    super(
        stream
          .filter(line -> !line.trim().startsWith(LINE_COMMENT))
    );
  }
}
