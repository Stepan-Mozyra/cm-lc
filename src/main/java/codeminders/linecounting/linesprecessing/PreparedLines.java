package codeminders.linecounting.linesprecessing;

import codeminders.linecounting.decorators.StringStreamDecorator;
import java.util.stream.Stream;

public final class PreparedLines extends StringStreamDecorator {

  public PreparedLines(final Stream<String> stream) {
    super(
        stream
            .map(line -> new NoEscapingQuoteLine(line).toString())
            .map(line -> new NoStringLiteralLine(line).toString())
            .map(line -> new NoInlineCommentBlockLine(line).toString())
    );
  }
}
