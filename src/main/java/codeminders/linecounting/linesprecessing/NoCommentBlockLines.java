package codeminders.linecounting.linesprecessing;

import codeminders.linecounting.decorators.StringStreamDecorator;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

public final class NoCommentBlockLines extends StringStreamDecorator {

  private final static String START = "/*";
  private final static String STOP = "*/";
  private final static String LINE_COMMENT = "//";

  public NoCommentBlockLines(final Stream<String> lines) {
    super(
        lines(lines)
    );
  }

  private static Stream<String> lines(final Stream<String> lines) {
    final AtomicReference<Boolean> inBlock = new AtomicReference<>(false);

    return
        lines
            .flatMap(NoCommentBlockLines::splitLines)
            .map(line -> transformLine(line, inBlock));
  }

  private static Stream<String> splitLines(final String line) {
    if (line.contains(START) && !line.equals(START) && notALineComment(line)) {
      return
          Stream.of(
              line.substring(0, line.indexOf(START)).trim(),
              line.substring(line.indexOf(START)).trim()
          );
    }

    return Stream.of(line);
  }

  private static String transformLine(
      final String line,
      final AtomicReference<Boolean> inBlock
  ) {
    return
        tryToTransformCommentBlockStart(line, inBlock)
            .orElseGet(
                () -> tryToTransformCommentBlockStop(line, inBlock)
                    .orElse(line)
            );
  }

  private static Optional<String> tryToTransformCommentBlockStart(
      final String line,
      final AtomicReference<Boolean> inBlock
  ) {
    if (line.startsWith(START) && notALineComment(line)) {
      inBlock.set(true);
      return
          Optional.of("");
    }

    return
        Optional.empty();
  }

  private static Optional<String> tryToTransformCommentBlockStop(
      final String line,
      final AtomicReference<Boolean> inBlock
  ) {
    if (inBlock.get()) {
      if (line.contains(STOP)) {
        inBlock.set(false);
        return
            Optional.of(
                line.substring(line.indexOf(STOP) + STOP.length()).trim()
            );
      } else {
        return
            Optional.of("");
      }
    }

    return
        Optional.empty();
  }

  private static boolean notALineComment(final String line) {
    return
        !line.startsWith(LINE_COMMENT);
  }
}
