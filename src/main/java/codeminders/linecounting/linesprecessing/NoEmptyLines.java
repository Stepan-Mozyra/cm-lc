package codeminders.linecounting.linesprecessing;

import codeminders.linecounting.decorators.StringStreamDecorator;
import java.util.stream.Stream;

public final class NoEmptyLines extends StringStreamDecorator {

  public NoEmptyLines(final Stream<String> stream) {
    super(
        stream
          .map(String::trim)
          .filter(line -> !line.isEmpty())
    );
  }
}
