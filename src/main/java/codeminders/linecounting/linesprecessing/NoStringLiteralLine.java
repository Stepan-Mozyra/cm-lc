package codeminders.linecounting.linesprecessing;

public final class NoStringLiteralLine {

  private final String line;

  public NoStringLiteralLine(final String line) {
    this.line = line;
  }

  @Override
  public String toString() {
    return
        line.replaceAll("\\\".*?\\\"", "");
  }
}
