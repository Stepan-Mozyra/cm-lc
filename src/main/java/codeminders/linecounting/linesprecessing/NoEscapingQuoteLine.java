package codeminders.linecounting.linesprecessing;

public final class NoEscapingQuoteLine {

  private final String line;

  public NoEscapingQuoteLine(final String line) {
    this.line = line;
  }

  @Override
  public String toString() {
    return
        line.replaceAll("\\\\\"", "");
  }
}
