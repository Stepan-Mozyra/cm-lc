package codeminders.linecounting.decorators;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;
import java.util.Spliterator;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;
import java.util.stream.Collector;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

public abstract class StringStreamDecorator implements Stream<String> {

  private final Stream<String> stream;

  public StringStreamDecorator(final Stream<String> stream) {
    this.stream = stream;
  }

  @Override
  public Stream<String> filter(final Predicate<? super String> predicate) {
    return stream.filter(predicate);
  }

  @Override
  public <R> Stream<R> map(final Function<? super String, ? extends R> mapper) {
    return stream.map(mapper);
  }

  @Override
  public IntStream mapToInt(final ToIntFunction<? super String> mapper) {
    return stream.mapToInt(mapper);
  }

  @Override
  public LongStream mapToLong(final ToLongFunction<? super String> mapper) {
    return stream.mapToLong(mapper);
  }

  @Override
  public DoubleStream mapToDouble(final ToDoubleFunction<? super String> mapper) {
    return stream.mapToDouble(mapper);
  }

  @Override
  public <R> Stream<R> flatMap(final Function<? super String, ? extends Stream<? extends R>> mapper) {
    return stream.flatMap(mapper);
  }

  @Override
  public IntStream flatMapToInt(final Function<? super String, ? extends IntStream> mapper) {
    return stream.flatMapToInt(mapper);
  }

  @Override
  public LongStream flatMapToLong(final Function<? super String, ? extends LongStream> mapper) {
    return stream.flatMapToLong(mapper);
  }

  @Override
  public DoubleStream flatMapToDouble(final Function<? super String, ? extends DoubleStream> mapper) {
    return stream.flatMapToDouble(mapper);
  }

  @Override
  public Stream<String> distinct() {
    return stream.distinct();
  }

  @Override
  public Stream<String> sorted() {
    return stream.sorted();
  }

  @Override
  public Stream<String> sorted(final Comparator<? super String> comparator) {
    return stream.sorted(comparator);
  }

  @Override
  public Stream<String> peek(final Consumer<? super String> action) {
    return stream.peek(action);
  }

  @Override
  public Stream<String> limit(final long maxSize) {
    return stream.limit(maxSize);
  }

  @Override
  public Stream<String> skip(final long n) {
    return stream.skip(n);
  }

  @Override
  public void forEach(final Consumer<? super String> action) {
    stream.forEach(action);
  }

  @Override
  public void forEachOrdered(final Consumer<? super String> action) {
    stream.forEachOrdered(action);
  }

  @Override
  public Object[] toArray() {
    return stream.toArray();
  }

  @Override
  public <A> A[] toArray(final IntFunction<A[]> generator) {
    return stream.toArray(generator);
  }

  @Override
  public String reduce(final String identity, final BinaryOperator<String> accumulator) {
    return stream.reduce(identity, accumulator);
  }

  @Override
  public Optional<String> reduce(final BinaryOperator<String> accumulator) {
    return stream.reduce(accumulator);
  }

  @Override
  public <U> U reduce(
      final U identity,
      final BiFunction<U, ? super String, U> accumulator,
      final BinaryOperator<U> combiner
  ) {
    return stream.reduce(identity, accumulator, combiner);
  }

  @Override
  public <R> R collect(
      final Supplier<R> supplier,
      final BiConsumer<R, ? super String> accumulator,
      final BiConsumer<R, R> combiner
  ) {
    return stream.collect(supplier, accumulator, combiner);
  }

  @Override
  public <R, A> R collect(final Collector<? super String, A, R> collector) {
    return stream.collect(collector);
  }

  @Override
  public Optional<String> min(final Comparator<? super String> comparator) {
    return stream.min(comparator);
  }

  @Override
  public Optional<String> max(final Comparator<? super String> comparator) {
    return stream.max(comparator);
  }

  @Override
  public long count() {
    return stream.count();
  }

  @Override
  public boolean anyMatch(final Predicate<? super String> predicate) {
    return stream.anyMatch(predicate);
  }

  @Override
  public boolean allMatch(final Predicate<? super String> predicate) {
    return stream.allMatch(predicate);
  }

  @Override
  public boolean noneMatch(final Predicate<? super String> predicate) {
    return stream.noneMatch(predicate);
  }

  @Override
  public Optional<String> findFirst() {
    return stream.findFirst();
  }

  @Override
  public Optional<String> findAny() {
    return stream.findAny();
  }

  @Override
  public Iterator<String> iterator() {
    return stream.iterator();
  }

  @Override
  public Spliterator<String> spliterator() {
    return stream.spliterator();
  }

  @Override
  public boolean isParallel() {
    return stream.isParallel();
  }

  @Override
  public Stream<String> sequential() {
    return stream.sequential();
  }

  @Override
  public Stream<String> parallel() {
    return stream.parallel();
  }

  @Override
  public Stream<String> unordered() {
    return stream.unordered();
  }

  @Override
  public Stream<String> onClose(final Runnable closeHandler) {
    return stream.onClose(closeHandler);
  }

  @Override
  public void close() {
    stream.close();
  }
}
