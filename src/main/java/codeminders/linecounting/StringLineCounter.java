package codeminders.linecounting;

import codeminders.linecounting.linesprecessing.NoCommentBlockLines;
import codeminders.linecounting.linesprecessing.NoCommentLines;
import codeminders.linecounting.linesprecessing.NoEmptyLines;
import codeminders.linecounting.linesprecessing.PreparedLines;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class StringLineCounter implements LineCounter {

  private final List<String> lines;

  public StringLineCounter(final List<String> lines) {
    this.lines = lines;
  }

  @Override
  public LineCount count() {
    final List<String> collect =
        new PreparedLines(
            lines.stream()
        ).collect(Collectors.toList());

    final List<String> collect1 =
        new NoCommentBlockLines(
            new PreparedLines(
                lines.stream()
            )
        ).collect(Collectors.toList());

    return
        new LineCount(
            Paths.get(""),
            new NoEmptyLines(
                new NoCommentLines(
                    new NoCommentBlockLines(
                        new PreparedLines(
                            lines.stream()
                        )
                    )
                )
            ).count(),
            Collections.emptyList()
        );
  }
}
