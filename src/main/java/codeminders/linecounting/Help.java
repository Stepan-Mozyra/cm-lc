package codeminders.linecounting;

import java.io.PrintStream;

final class Help implements Displayable {

  @Override
  public void display(final PrintStream out) {
    out.println("You can specify a java source code file, or folder as an argument.\n");
    out.println("This help is also a \"testing template\".");

    /*
      comment block
     */
    int a = 1;
    int b = 2 /* inline comment block */;
    /* line with the comment block */
    int c = 3; /*
      jet another comment block
    */ int d = 4;

    out.println();
  }
}
