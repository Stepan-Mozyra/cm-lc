package codeminders.linecounting;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

final class ListLineCounter implements LineCounter {

  private final List<Path> paths;

  ListLineCounter(final List<Path> paths) {
    this.paths = paths;
  }

  @Override
  public LineCount count() {
    return
        paths
          .stream()
          .map(this::lineCounter)
          .filter(Optional::isPresent)
          .map(Optional::get)
          .map(LineCounter::count)
          .reduce(
              new LineCount(
                  Paths.get("Total"),
                  0,
                  Collections.emptyList()
              ),
              LineCount::add
          );
  }

  private Optional<LineCounter> lineCounter(final Path path) {
    if (path.toFile().isDirectory()) {
      return
          Optional.of(new FolderLineCounter(path));
    } else if (path.toString().endsWith(".java")) {
      return
          Optional.of(new FileLineCounter(path));
    } else {
      return
          Optional.empty();
    }
  }
}
