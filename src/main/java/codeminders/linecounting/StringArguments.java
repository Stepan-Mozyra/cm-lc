package codeminders.linecounting;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

final class StringArguments implements Arguments {

  private final String[] args;

  StringArguments(final String[] args) {
    this.args = Stream.of(args)
        .map(String::trim)
        .filter(arg -> !arg.equals(""))
        .collect(Collectors.toList())
        .toArray(new String[]{});
  }

  @Override
  public boolean noArgProvided() {
    return
        args.length == 0;
  }

  @Override
  public boolean helpRequired() {
    return
        Arrays.asList(args)
            .contains("--help");
  }

  @Override
  public List<Path> list() {
    return
        Stream.of(args)
            .map(Paths::get)
            .collect(Collectors.toList());
  }
}
