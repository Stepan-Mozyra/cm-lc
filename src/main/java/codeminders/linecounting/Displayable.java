package codeminders.linecounting;

import java.io.PrintStream;

public interface Displayable {

  void display(PrintStream out);

}
