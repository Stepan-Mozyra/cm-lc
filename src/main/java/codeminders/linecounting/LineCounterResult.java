package codeminders.linecounting;

import java.io.PrintStream;

final class LineCounterResult implements Displayable {

  private final LineCounter lineCounter;

  LineCounterResult(final LineCounter lineCounter) {
    this.lineCounter = lineCounter;
  }

  @Override
  public void display(final PrintStream out) {
    display(
        "",
        lineCounter.count(),
        out
    );
  }

  private void display(
      final String indent,
      final LineCount count,
      final PrintStream out
  ) {
    out.println(
        indent +
            count.path().getFileName()
            + ": "
            + count.number()
    );

    count.counts().forEach(subCount ->
        display(
            indent + "\t",
            subCount,
            out
        )
    );
  }
}
