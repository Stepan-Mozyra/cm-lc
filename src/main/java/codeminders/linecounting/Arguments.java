package codeminders.linecounting;

import java.nio.file.Path;
import java.util.List;

interface Arguments {

  boolean noArgProvided();

  boolean helpRequired();

  List<Path> list();

}
