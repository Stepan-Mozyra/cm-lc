package codeminders.linecounting;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class LineCount {

  private final Path path;
  private final long count;
  private final List<LineCount> counts;

  public LineCount(
      final Path path,
      final long count,
      final List<LineCount> counts
  ) {
    this.path = path;
    this.count = count;
    this.counts = counts;
  }

  public LineCount add(final LineCount lineCount) {
    final ArrayList<LineCount> addedCounts = new ArrayList<>(counts);
    addedCounts.add(lineCount);

    return
        new LineCount(
            path,
            count,
            addedCounts
        );
  }

  public Path path() {
    return
        path;
  }

  public long number() {
    return
        count +
            counts.stream().mapToLong(LineCount::number).sum();
  }

  public List<LineCount> counts() {
    return
        counts;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LineCount lineCount = (LineCount) o;
    return count == lineCount.count &&
        path.equals(lineCount.path) &&
        counts.equals(lineCount.counts);
  }

  @Override
  public int hashCode() {
    return Objects.hash(path, count, counts);
  }

  @Override
  public String toString() {
    return "LineCount{" +
        "path=" + path +
        ", count=" + count +
        ", counts=" + counts +
        '}';
  }
}
