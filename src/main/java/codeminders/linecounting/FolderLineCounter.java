package codeminders.linecounting;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class FolderLineCounter implements LineCounter {

  private final Path folder;

  public FolderLineCounter(final Path folder) {
    this.folder = folder;
  }

  @Override
  public LineCount count() {
    return
        new LineCount(
            folder,
            0,
            new ListLineCounter(
                ls(folder)
                    .map(file -> Paths.get(folder.toString(), file))
                    .collect(Collectors.toList())
            ).count().counts()
        );
  }

  private Stream<String> ls(final Path parent) {
    final String[] list = parent.toFile().list();
    return
        list == null
            ? Stream.empty()
            : Stream.of(list);
  }
}
