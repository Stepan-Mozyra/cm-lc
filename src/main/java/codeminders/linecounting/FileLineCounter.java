package codeminders.linecounting;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

final class FileLineCounter implements LineCounter {

  private final Path file;

  FileLineCounter(final Path file) {
    this.file = file;
  }

  @Override
  public LineCount count() {
    return
        new LineCount(
            file,
            new StringLineCounter(loadFile())
                .count()
                .number(),
            Collections.emptyList()
        );
  }

  private List<String> loadFile() {
    try {
      return
          Files.readAllLines(file, StandardCharsets.UTF_8);
    } catch (final IOException e) {
      // TODO: figure out what to do
      return
          Collections.emptyList();
    }
  }
}
