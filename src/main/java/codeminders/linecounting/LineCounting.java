package codeminders.linecounting;

import java.io.PrintStream;

public final class LineCounting implements Displayable {

	private final Arguments arguments;
	private final Displayable result;
	private final Displayable help;

  public static void main(final String[] args) {
		final StringArguments arguments = new StringArguments(args);

		new LineCounting(
				arguments,
				new LineCounterResult(
						new ListLineCounter(
								arguments.list()
						)
				),
				new Help()
		).display(System.out);
  }

	public LineCounting(
			final Arguments arguments,
			final Displayable result,
			final Displayable help
	) {
		this.arguments = arguments;
		this.result = result;
		this.help = help;
	}

	@Override
	public void display(final PrintStream out) {
  	if (arguments.noArgProvided() || arguments.helpRequired()) {
  		help.display(out);
		} else {
  		result.display(out);
		}
	}
}
